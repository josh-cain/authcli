#!/usr/bin/env node

"use strict";

const commander = require("commander");
const request = require('request');
const { exit, argv } = process;
const homedir = require('os').homedir();
const fs = require('fs');
const opn = require('opn');

// TODO more control over this...
//process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

// TODO move this to init routine somewhere...
const contextFilePath = homedir + '/.auth-context.json';
var authContext = {};
if (fs.existsSync(contextFilePath)) {
  console.log('loading context file: ' + contextFilePath);
  const rawContext = fs.readFileSync(contextFilePath);
  authContext = JSON.parse(rawContext);
  console.log(JSON.stringify(authContext));
} else {
  console.log('could not find context file: ' + contextFilePath + ', will start fresh.');
}

// TODO make params required?
commander.command('device-start')
    .description('Initiates the device flow.  This sends the initial request as if it were the device being set up.')
    .option('-s, --auth-server [url]', 'Auth Server URL (i.e. https://jcain.auth0.com)')
    .option('-c, --client-id [client_id]', 'Client ID for the device activation request')
    .action((cmd) => {
      const postData = {form:{
        'client_id': cmd.clientId,
        'scope': 'openid'
      }};
      console.log('Attempting to POST data: ' + JSON.stringify(postData) + ' to server: ' + cmd.authServer);
      request.post(
        cmd.authServer + '/oauth/device/code',
        postData,
        (err, res, body) => {
          if (err) {
            console.error('error occurred attempting to start device flow: ' + err);
            exit(1);
          }
          // TODO do something with response
          console.log(`STATUS: ${res.statusCode}`);
          console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
          console.log('recieved final body: \n' + body);
          const deviceCodeInit = JSON.parse(body);
          console.log('device code: ' + deviceCodeInit.device_code);
          console.log('user_code: ' + deviceCodeInit.user_code);
          console.log('verification_uri_complete: ' + deviceCodeInit.verification_uri_complete);

          authContext.clientId = cmd.clientId;
          authContext.authServer = cmd.authServer;
          authContext.deviceCode = deviceCodeInit.device_code;
          authContext.user_code = deviceCodeInit.user_code;
          authContext.verification_uri_complete = deviceCodeInit.verification_uri_complete;
          fs.writeFileSync(contextFilePath, JSON.stringify(authContext));
          exit(0);
        });
    });

commander.command('device-poll')
  .description('Uses the current device code to poll the token endpoint to see if the user confirmed.')
  .option('-s, --auth-server [url]', 'Auth Server URL (i.e. https://jcain.auth0.com/)')
  .option('-c, --client-id [client_id]', 'Client ID for the device activation request')
  .option('-d, --device-code [device_code]', 'Device code used for getting a token')
  .action((cmd) => {
    console.log('you invoked the device-poll action with auth server: ' + cmd.authServer);
    // TODO make these more flexible - real URL class @ node potentially?
    const authServerBaseUrl = cmd.authServer ? cmd.authServer : authContext.authServer;
    const postData = {form: {
      'grant_type': 'urn:ietf:params:oauth:grant-type:device_code',
      'device_code': cmd.deviceCode ? cmd.deviceCode : authContext.deviceCode,
      'client_id': cmd.clientId ? cmd.clientId : authContext.clientId
    }}
    request.post(
      authServerBaseUrl + '/oauth/token',
      postData,
      (err, res, body) => {
        if (err) {
          console.error('error occurred attempting to start device flow: ' + err);
          exit(1);
        }

        console.log(`STATUS: ${res.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        console.log('recieved final body: \n' + body);

        // TODO save context @ file
        exit(0);
      }
    )
  });

commander.command('device-auth')
  .description('Uses the current device context to launch an auth flow')
  .action((cmd) => {
    console.log('you invoked the device-auth action with auth server: ' + cmd.authServer);
    // TODO real checking for pre-conditions here.  I.E. useful message if device flow not init'd
    opn(authContext.verification_uri_complete);
  });

// TODO display help text when no commands match
commander.parse(argv);
