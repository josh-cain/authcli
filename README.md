# authcli

CLI meant for quick-and-dirty auth testing
=======
# auth

tool for interacting with auth flows

## Installation

### Yarn

```shell
yarn global add "auth"
```

### Npm

```shell
npm install --global "auth"
```

## Usage

## Related
